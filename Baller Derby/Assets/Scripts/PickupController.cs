﻿using UnityEngine;
using System.Collections;

public class PickupController : MonoBehaviour {
    private bool set;
    private float t;
    public float respawn;

    void Start()
    {
        set = true;
    }
    void Update()
    {
        if (!set && (Time.time - t) >= respawn) //object has been inactive for <respawn> seconds
        {
            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<BoxCollider>().enabled = true;
            set = true;
        }
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
       
    }
    void LateUpdate()
    {
        if (set && !GetComponent<MeshRenderer>().enabled) //object was set inactive this frame
        {
            t = Time.time;
            set = false;
        }
    }
}
