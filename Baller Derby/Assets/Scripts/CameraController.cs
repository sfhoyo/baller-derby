﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public GameObject player;
    private Vector3 offset;
	// Use this for initialization
	void Start () {
        offset = transform.position - player.transform.position;
	}
	
	// LateUpdate runs after Update(), so we know absolutely that player has moved, no race cond
	void LateUpdate () {
        transform.position = player.transform.position + offset;
	}
}
