﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerController : NetworkBehaviour
{

    public float speed; //speed multiplier
    public float growthRate; //affects growth of force, mass, and size (>0 is increase)
    public GameObject playerCamera; //prefab

    private GameObject playerCam;
    private Rigidbody rb;
    private float mForce;
    private float r;
    Vector3 spectateAng = new Vector3(90.0f, 0.0f, 0.0f);
    Vector3 spectatePos = new Vector3(0.0f, 20.0f, 0.0f);

    public override void OnStartLocalPlayer()
    {
        
        playerCam = Instantiate(playerCamera);
        playerCam.GetComponent<CameraController>().player = this.gameObject;
        
        if (isServer)
        {
            GetComponent<MeshRenderer>().material.color = Color.red;
        }
    }
    public void DestroyCamera()
    {
        Destroy(playerCam);
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        mForce = 1.0f;
        r = transform.localScale.x;
    
    }
    void Update()
    {
        if (checkLoseCon())
        {
            playerCam.GetComponent<CameraController>().enabled = false;
            playerCam.transform.eulerAngles = spectateAng;
            playerCam.transform.position = spectatePos;
        }    
           
    }

    void FixedUpdate() //physics
    {
        if (!isLocalPlayer)
            return;
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 uForce = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(uForce*mForce*speed);
    }
    void OnTriggerEnter(Collider other) //When the player enters something, usually eating pickup
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            float growthRatio = Mathf.Pow(r + growthRate, 3) / Mathf.Pow(r, 3);
            float mOld = rb.mass;
            
            //Physics
            mForce *= growthRatio*(r/(growthRate+r)); //Ensures accelaration decreases, but not too much
            rb.mass *= growthRatio;
            r += growthRate;
            transform.localScale = new Vector3(r, r, r);
            rb.velocity *= (mOld / rb.mass); //conserve momentum

            //Hide pickup
            other.gameObject.GetComponent<MeshRenderer>().enabled = false;
            other.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
    bool checkLoseCon()
    {
        if (transform.position.y < 0) //just to provide leeway
            return true;
        return false;
    }
}
