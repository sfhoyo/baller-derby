﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class Netcode : NetworkManager {

    // Use this for initialization
    public override void OnServerRemovePlayer(NetworkConnection conn, UnityEngine.Networking.PlayerController player)
    {
        if(player.gameObject != null)
        {
            player.gameObject.GetComponent<PlayerController>().DestroyCamera();
        }
        base.OnServerRemovePlayer(conn, player);
    }
}
